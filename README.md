## Aplicação - Gerenciamento

Essa aplicação simples tem o intuito de organizar os relatórios de uma empresa, tarefas e lista de funcionários.

Para rodar faça:

`yarn add react-script`

e depois

`yarn start`.
