import React from 'react';
import { Admin, Resource } from 'react-admin';

import { myTheme} from './Layout/MyTheme'
import { myLayout } from './Layout/MyLayout'

import { AlbumList} from './Fotos/index';
import { AlbumShow } from './Fotos/AlbumShow/index';
import { TarefaList, TarefaCreate,TarefaEdit } from './Tarefas/index';
import { EmployeeList } from './Funcionários/index'
import { EmployeeShow } from './Funcionários/FuncionáriosShow/index.js';
import { MyLoginPage } from './Login/index.js'
import authProvider from './Login/authProvider';

import jsonServerProvider from 'ra-data-json-server';

const dataProvider = jsonServerProvider('https://jsonplaceholder.typicode.com');


const App = () => (
  <Admin
      authProvider={authProvider}
      loginPage={MyLoginPage}
      dataProvider={dataProvider}
      theme={myTheme}
      layout={myLayout}
      >
      <Resource name="albums" list={AlbumList} show={AlbumShow}
            options={{ label: 'Relatórios' }}/>
      <Resource name="todos" list={TarefaList} create={TarefaCreate} edit={TarefaEdit}
            options={{ label: 'Tarefas' }}/>
      <Resource name="users" list={EmployeeList} show={EmployeeShow} />
      <Resource name="photos" />
      <Resource name= "posts" />

  </Admin>
);

export default App;
