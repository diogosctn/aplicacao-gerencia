
import React from 'react';
import { List,  Datagrid, TextField, ReferenceField  } from 'react-admin';
import { Edit, Create, SimpleForm,TextInput, ReferenceInput, SelectInput } from 'react-admin';
import { Filter, BooleanField } from 'react-admin';

    
const TarefaFilter = (props) => (
    <Filter {...props}>
        <TextInput label="Search" source="q" alwaysOn />
    </Filter>
);

const TarefaTitle = ({ record }) => {
    return <span>Tarefas</span>;
};

export const TarefaList = (props) => (
    <List title={<TarefaTitle/>} filters={<TarefaFilter />} {...props}>
            <Datagrid rowClick="edit">
                <ReferenceField label="User" source="userId" reference="users">
                    <TextField source="name" />
                </ReferenceField>
                <TextField source="title" />
                <BooleanField source="completed" valueLabelTrue="Completo" valueLabelFalse="Incompleto"/>
            </Datagrid>
    </List>
);

const TarefaCreateTitle = ({ record }) => {
    return <span>Criar nova tarefa</span>;
};

const TarefaEditTitle = ({ record }) => {
    return <span> Tarefa: {record ? `"${record.title}"` : ''} </span>;
};

export const TarefaCreate = props => (
    <Create title={<TarefaCreateTitle/>} {...props}>
        <SimpleForm>
            <ReferenceInput source="userId" reference="users">
                <SelectInput optionText="name" />
            </ReferenceInput>
            <TextInput source="title" />
        </SimpleForm>
    </Create>
);

export const TarefaEdit = props => (
    <Edit title={<TarefaEditTitle />} {...props}>
        <SimpleForm>
            <ReferenceInput source="userId" reference="users">
                <SelectInput optionText="name" />
            </ReferenceInput>
            <TextInput source="title" />
        </SimpleForm>
    </Edit>
);