import React from 'react';
import { ReferenceField, TextField  } from 'react-admin';
import { ImageField, TabbedShowLayout, Show, Tab} from 'react-admin';
import { ReferenceManyField, SingleFieldList} from 'react-admin';

const AlbumTitle = ({ record }) => {
        return <span>Relatório: {record ? `"${record.title}"` : ''}</span>;
    };

export const AlbumShow = props => (
    <Show title={<AlbumTitle />} {...props}>
        <TabbedShowLayout>
            <Tab label="Descrição">
                <TextField label="Título" source="title" />
                <ReferenceField label="Descrição" source="userId" reference="posts" link={false}>
                    <TextField source="body" />
                </ReferenceField>
                <ReferenceField label="Funcionário" source="userId" reference="users">
                        <TextField source="name" />
                </ReferenceField>
            </Tab>
            <Tab label="Imagens">
            <ReferenceManyField label="" reference="photos" target="albumId">
                <SingleFieldList>
                <ImageField source="url"/>
                </SingleFieldList>
            </ReferenceManyField>
            </Tab>
        </TabbedShowLayout>
    </Show>
);