import React from 'react';
import { List, Datagrid, ReferenceField, TextField  } from 'react-admin';
import { ImageField, SimpleShowLayout, Show, ShowButton } from 'react-admin'
import { TextInput,Filter } from 'react-admin';
import { ReferenceManyField, SingleFieldList} from 'react-admin';

const AlbumTitle = ({ record }) => {
        return <span>: {record ? `"${record.title}"` : ''}</span>;
    };

const PageTitle = ({ record }) => {
    return <span>Relatório</span>;
};

export const AlbumShow = props => (
    <Show title={<AlbumTitle />} {...props}>
        <SimpleShowLayout>
            <ReferenceField label="Description" source="userId" reference="posts" link={false}>
                <TextField source="body" />
            </ReferenceField>
            <ReferenceManyField label="Photos" reference="photos" target="albumId">
                <SingleFieldList>
                <ImageField source="url"/>
                </SingleFieldList>
            </ReferenceManyField>
        </SimpleShowLayout>
    </Show>
);

const AlbumFilter = (props) => (
    <Filter  {...props}>
        <TextInput label="Search" source="q" alwaysOn />

    </Filter>
);

export const AlbumList = (props) => (
    <List title={<PageTitle />} filters={<AlbumFilter />} {...props}>
            <Datagrid >
                <ReferenceField label="User" source="userId" reference="users">
                        <TextField source="name" />
                </ReferenceField>
                <TextField source="title" />
                <ShowButton />
            </Datagrid>
    </List>
);
