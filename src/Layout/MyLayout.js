import React from 'react';
import { Layout } from 'react-admin';
import { MyAppBar } from './MyAppBar';
import { MyMenu } from './MyMenu.js';

export const myLayout = props => <Layout
    {...props}
    appBar={MyAppBar}
    menu={MyMenu}
/>;