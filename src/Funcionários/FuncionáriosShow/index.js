import React from 'react';
import { ReferenceManyField, TextField, Show } from 'react-admin';
import { TabbedShowLayout, Tab, Datagrid, BooleanField } from 'react-admin'

const EmployeeTitle = ({ record }) => {
    return <span>{record ? `"${record.name}"` : ''}</span>;
};

export const EmployeeShow = props => (
    <Show title={<EmployeeTitle />}{...props}>
        <TabbedShowLayout>
            <Tab label="Endereço">
                <TextField label="Cidade" source="address.city" />
                <TextField label="Rua" source="address.street" />
                <TextField label="Apartamento" source="address.suite" />
            </Tab>
            <Tab label="Atividades">        
                <ReferenceManyField label="" reference="todos" target="userId">
                    <Datagrid>
                    <TextField label="" source="title"/>
                    <BooleanField label="" source="completed" valueLabelTrue="Completo" valueLabelFalse="Incompleto"/>
                    </Datagrid>
                </ReferenceManyField>
            </Tab>
            <Tab label="Relatórios">        
                <ReferenceManyField label="" reference="albums" target="userId">
                    <Datagrid rowClick='show'>
                    <TextField label="" source="title"/>
                    </Datagrid>
                </ReferenceManyField>
            </Tab>
        </TabbedShowLayout>
    </Show>
);