import React from 'react';
import { List, Datagrid, TextField, Show, SimpleList } from 'react-admin';
import { NumberField,TextInput,Filter } from 'react-admin';
import { ReferenceManyField, EmailField, TabbedShowLayout, Tab, BooleanField} from 'react-admin';
import { useMediaQuery } from '@material-ui/core';

const EmployeeResumeShow = props => (
    <Show title=" " {...props}>
        <TabbedShowLayout>
            <Tab label="Endereço">
                <TextField label="Cidade" source="address.city" />
                <TextField label="Rua" source="address.street" />
                <TextField label="Apartamento" source="address.suite" />
            </Tab>
            <Tab label="Atividades">        
                <ReferenceManyField label="" reference="todos" target="userId">
                    <Datagrid>
                    <TextField label="" source="title"/>
                    <BooleanField label="" source="completed" valueLabelTrue="Completo" valueLabelFalse="Incompleto"/>
                    </Datagrid>
                </ReferenceManyField>
            </Tab>
            <Tab label="Relatórios">        
                <ReferenceManyField label="" reference="albums" target="userId">
                    <Datagrid rowClick='show'>
                    <TextField label="" source="title"/>
                    </Datagrid>
                </ReferenceManyField>
            </Tab>
        </TabbedShowLayout>
    </Show>
);

const EmployeeFilter = (props) => (
    <Filter  {...props}>
        <TextInput label="Search" source="q" alwaysOn />

    </Filter>
);

const EmployeeTitle = ({ record }) => {
    return <span>Funcionários</span>;
};

export const EmployeeList = (props) => {
    const isSmall = useMediaQuery(theme => theme.breakpoints.down('sm'));
    return (
        <List title={<EmployeeTitle/>} filters={<EmployeeFilter />} {...props}>
            {isSmall ? (
                <SimpleList
                    primaryText={record => record.name}
                    secondaryText={record => record.id}
                    tertiaryText={record => record.phone}
                    linkType={'show'}
                />
                ) : (
                <Datagrid expand={<EmployeeResumeShow />}>
                    <TextField label="Identificação" source="id" />
                    <TextField label="Nome" source="name" />
                    <NumberField label="Telefone "source="phone" />
                    <EmailField  label="E-mail" source="email" />
                </Datagrid>
            )}
        </List>
    );
}